<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $product = new \App\Product([
            'imagePath' => 'images/tunel.jpg',
            'title' => 'El túnel',
            'description' => 'El Túnel es una novela corta argentina escrita por Ernesto Sábato en 1948. Juan Pablo Castel, personaje principal y narrador, cuenta desde la cárcel los motivos que lo llevaron a asesinar a María Iribarne, su amante.',
            'price' => 120
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'images/tochtli.jpg',
            'title' => 'El conejo en la cara de la luna',
            'description' => 'Alfredo López Austin, uno de los más destacados estudiosos del pensamiento y la religión prehispánicas, autor de grandes tratados e indagaciones exhaustivas, ofrece aquí una muestra de sus hallazgos. Y lo hace en el tono de quien conversa, con humor y ánimo de entretener, a la vez echando mano de toda su erudición.',
            'price' => 120
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'images/abalorios.jpg',
            'title' => 'El juego de los abalorios',
            'description' => 'El juego de los abalorios es una novela escrita por Hermann Hesse y publicada en 1943. Fue la última de sus obras editada en vida del autor, tres años antes de recibir el Premio Nobel de Literatura.',
            'price' => 120
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'images/siddhartha.jpg',
            'title' => 'Siddhartha',
            'description' => 'Siddhartha es una novela alegórica escrita por Hermann Hesse en 1922 tras la primera guerra mundial. La misma relata la vida de un hombre hindú llamado Siddhartha.',
            'price' => 120
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'images/moscas.jpg',
            'title' => 'El señor de las moscas',
            'description' => 'El señor de las moscas es la primera y más célebre novela de William Golding. Publicada en 1954, se considera un clásico de la literatura inglesa de postguerra.',
            'price' => 120
        ]);
        $product->save();
    }
}

@extends('layouts.master')

@section('title')
  Laravel Shopping Cart
@endsection

@section('content')

  @foreach($products->chunk(3) as $productChunk)

  <div class="row">
  
    @foreach($productChunk as $product)

      <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top img-fluid img-thumbnail" src="{{ URL::to($product->imagePath) }}" alt="{{ $product->title }}">
          <div class="card-body">
            <h5 class="card-title">{{ $product->title }}</h5>
            <p class="card-text product-description">{{ $product->description }}</p>
            <div class="row">
              <div class="col price">${{ $product->price }}</div>
              <div class="col-sm-6 col-md-4"><a href="#" class="btn btn-success float-right">Add to Cart</a></div>
            </div>
          </div>
        </div>
      </div>

    @endforeach

  </div>

  @endforeach

@endsection